//Метод forEach обращается к каждому элементу массива по очереди, итерируется до конца массива.


let items = ['hello', 'world', 23, '23', null, undefined];
const allTypes = ['string', 'number', 'boolean', 'object', 'bigint', 'undefined'];


// фильтр с помощью filter

function filterBy (arr, type){
     let result = arr.filter(item => (typeof item !== type ));
     return result;
};


//фильтр с помощью forEach

//function filterBy (arr, type) {
//     let result = [];
//     arr.forEach(function(elem){
//          if (typeof elem !== type) {
//               result.push(elem);
//      };
//}); 
//     return result;
//};


allTypes.forEach(type => console.log(filterBy(items, type)));